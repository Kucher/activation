<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Activation token column
    |--------------------------------------------------------------------------
    |
    | Specify the column name for the activation token on your users provider model
    |
    */
    'column' => 'activation_token',

    /*
    |--------------------------------------------------------------------------
    | Activation notification class
    |--------------------------------------------------------------------------
    |
    | Specify the class for the activation notification
    |
    */
    'notification' => '\\App\\Notifications\\Activation'
];
