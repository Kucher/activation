<?php
namespace Enso\Activation\Facades;

use Illuminate\Support\Facades\Facade;

class Activation extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'activation';
    }

    public static function routes()
    {
        static::$app->make('router')->get('activate/{user}/{token}', 'Auth\ActivationController@activate')->name('activate');
    }
}
