<?php
namespace Enso\Activation\Controllers;

use Illuminate\Foundation\Auth\User;

class ActivationController
{
    /**
     * Try to activate the user with the specified token
     *
     * @param User $user
     * @param null $token
     * @return bool
     */
    public function activate(User $user, $token = null)
    {
        if ($user && $user->{config('activation.column')} === $token) {
            $user->{config('activation.column')} = null;
            $user->save();

            return true;
        }

        return false;
    }

    /**
     * Refresh activation for currently logged in user
     *
     * @return bool
     */
    public function refresh()
    {
        auth()->user()->{config('activation.column')} = uniqid();
        auth()->user()->save();

        $notificationClass = config('activation.notification');

        auth()->user()->notify(new $notificationClass(auth()->user()));

        auth()->logout();

        return true;
    }
}
