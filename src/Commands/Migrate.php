<?php
namespace Enso\Activation\Commands;

use Illuminate\Console\Command;
use Schema;

class Migrate extends Command
{
    protected $signature = 'activation:migrate 
        {--database= : The database connection to use.}';

    protected $description = 'Add the required activation token column to your users provider table';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $connection = $this->option('database') ?: env('DB_CONNECTION', 'mysql');
        $modelClass = config('auth.providers.users.model');
        $model = new $modelClass();
        $table = $model->getTable();

        if (!Schema::connection($connection)->hasColumn($table, config('activation.column'))) {
            Schema::connection($connection)->table($model->getTable(), function($table) {
                $table->string(config('activation.column'))->nullable();
            });

            $this->info('> Activation token column "'.config('activation.column').'" successfully added on users table "'.$table.'"');
        } else {
            $this->comment('> Activation token column "'.config('activation.column').'" already exists on users table "'.$table.'" ... skipped');
        }

        $this->info('>>> Finished table modifications for activation.');
    }
}
