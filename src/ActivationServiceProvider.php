<?php
namespace Enso\Activation;

use Illuminate\Support\ServiceProvider;
use Enso\Activation\Commands\Migrate;
use Enso\Activation\Controllers\ActivationController;

class ActivationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/activation.php' => config_path('activation.php'),
            __DIR__.'/app' => app_path()
        ]);

        if ($this->app->runningInConsole()) {
            $this->commands([
                Migrate::class,
            ]);
        }
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/config/activation.php', 'activation');

        $this->app->bind('activation', function() {
            return new ActivationController();
        });
    }
}
