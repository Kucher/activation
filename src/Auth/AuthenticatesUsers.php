<?php
namespace Enso\Activation\Auth;

use Illuminate\Http\Request;

trait AuthenticatesUsers
{
    use \Illuminate\Foundation\Auth\AuthenticatesUsers;

    /**
     * Avoid login if the user account is not activated yet
     *
     * @param Request $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        if ($this->guard()->attempt($this->credentials($request), $request->has('remember'))) {
            if ($this->guard()->user()->{config('activation.column')} === null) {
                return true;
            } else {
                auth()->logout();
            }
        }

        return false;
    }
}
