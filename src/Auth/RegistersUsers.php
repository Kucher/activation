<?php

namespace Enso\Activation\Auth;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

trait RegistersUsers
{
    use \Illuminate\Foundation\Auth\RegistersUsers;

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    /**
     * Sets the activation token after registration
     *
     * @param Request $request
     * @param $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function registered(Request $request, $user)
    {
        $user->{config('activation.column')} = uniqid();
        $user->save();

        $notificationClass = config('activation.notification');

        $user->notify(new $notificationClass($user));

        return redirect($this->redirectPath());
    }
}
